﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace excel_helper
{


    public class DiaryExcelHelper : IDisposable
    {
        const string _sheetName = "个人工作日志";

        public int RowToInsert { get; set; }
        public string DatePosition { get; set; }
        public string NumberPosition { get; set; }
        public string DepartPosition { get; set; }
        public string NamePosition { get; set; }

        public int RowsOfEachItem { get; set; }

        ExcelPackage _excelPackage;

        ExcelWorksheet _workSheet;

        public DiaryExcelHelper(string filename)
        {
            Console.WriteLine(filename);

            System.IO.FileInfo fi = new System.IO.FileInfo(filename);

            _excelPackage = new ExcelPackage(fi);
            _workSheet = _excelPackage.Workbook.Worksheets[_sheetName];
        }


        private void SetStaffName(string name)
        {
            _workSheet.SetValue(NamePosition, name);
        }

        private void SetDepart(string depart)
        {
            _workSheet.SetValue(DepartPosition, depart);
        }

        private void SetNumber(string staffNumber)
        {
            _workSheet.SetValue(NumberPosition, staffNumber);
        }

        private void SetDate(string date)
        {
            _workSheet.SetValue(DatePosition, date);
        }

        void setRange(int fromRow, int fromCol, int toRow, int toCol)
        {
            ExcelRange range;
            range = _workSheet.Cells[fromRow, fromCol, toRow, toCol];
            range.Merge = true;
            range.Style.WrapText = true;
            range.Style.ShrinkToFit = true;
            range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            var border = range.Style.Border;
            border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
        }


        public void setBorder(int fromRow, int fromCol, int toRow, int toCol,string borderItemStr ,ExcelBorderStyle style)
        {
            ExcelRange range;
            range = _workSheet.Cells[fromRow, fromCol, toRow, toCol];
            var border = range.Style.Border;

            ExcelBorderItem borderItem = border.Top;

            switch (borderItemStr)
            {
                case "top":
                    borderItem = border.Top;
                    break;
                case "left":
                    borderItem = border.Left;
                    break;
                case "right":
                    borderItem = border.Right;
                    break;
                case "bottom":
                    borderItem = border.Bottom;
                    break;
            }

            borderItem.Style = style;
        }

        public void Insert_m_item(m_job_item diary)
        {
            ExcelRange range;

            //维护编号
            _workSheet.InsertRow(RowToInsert, 7);
            _workSheet.Cells[RowToInsert, 2].Value = "维护编号";
            setRange(RowToInsert, 2, RowToInsert, 2);
            _workSheet.Cells[RowToInsert, 3].Value = diary.number;
            setRange(RowToInsert, 3, RowToInsert, 5);

            //维护类型
            _workSheet.Cells[RowToInsert, 6].Value = "维护类型";
            setRange(RowToInsert, 6, RowToInsert, 8);
            _workSheet.Cells[RowToInsert, 9].Value = diary.m_type;
            setRange(RowToInsert, 9, RowToInsert, 9);

            //单位
            _workSheet.Cells[RowToInsert + 1, 2].Value = "单位";
            setRange(RowToInsert + 1, 2, RowToInsert + 1, 2);
            _workSheet.Cells[RowToInsert + 1, 3].Value = diary.client;
            setRange(RowToInsert + 1, 3, RowToInsert + 1, 5);

            //产品
            _workSheet.Cells[RowToInsert + 1, 6].Value = "产品";
            setRange(RowToInsert + 1, 6, RowToInsert + 1, 6);
            _workSheet.Cells[RowToInsert + 1, 7].Value = diary.project_name;
            setRange(RowToInsert + 1, 7, RowToInsert + 1, 9);

            //联系人
            _workSheet.Cells[RowToInsert + 2, 2].Value = "联系人";
            setRange(RowToInsert + 2, 2, RowToInsert + 2, 2);
            _workSheet.Cells[RowToInsert + 2, 3].Value = diary.contract_man;
            setRange(RowToInsert + 2, 3, RowToInsert + 2, 5);

            //联系方式
            _workSheet.Cells[RowToInsert + 2, 6].Value = "联系方式";            
            _workSheet.Cells[RowToInsert + 2, 7].Value = diary.contract_way;
            setRange(RowToInsert + 2, 7, RowToInsert + 2, 9);

            //客户提出问题
            _workSheet.Cells[RowToInsert + 3, 2].Value = "客户提出问题";
            setRange(RowToInsert + 3, 2, RowToInsert + 4, 2);

            _workSheet.Cells[RowToInsert + 3, 3].Value = diary.client_question;
            setRange(RowToInsert + 3, 3, RowToInsert + 3, 9);

            //要求时间
            _workSheet.Cells[RowToInsert + 4, 3].Value = "要求时间";
            _workSheet.Cells[RowToInsert + 4, 4].Value = diary.client_time;
            setRange(RowToInsert + 4, 4, RowToInsert + 4, 9);

            //处理内容
            _workSheet.Cells[RowToInsert + 5, 2].Value = "处理内容";
            setRange(RowToInsert + 5, 2, RowToInsert + 6, 2);

            _workSheet.Cells[RowToInsert + 5, 3].Value = diary.handle_content;
            setRange(RowToInsert + 5, 3, RowToInsert + 5, 5);

            //处理方式
            _workSheet.Cells[RowToInsert + 5, 6].Value = diary.handle_method;
            setRange(RowToInsert + 5, 6, RowToInsert + 6, 9);

            //用时
            _workSheet.Cells[RowToInsert + 6, 3].Value = "用时";
            setRange(RowToInsert + 6, 3, RowToInsert + 6, 3);
            _workSheet.Cells[RowToInsert + 6, 4].Value = diary.time_use;
            setRange(RowToInsert + 6, 4, RowToInsert + 6, 5);

            //是否完成
            _workSheet.Cells[RowToInsert + 6, 6].Value = "是否完成";
            _workSheet.Cells[RowToInsert + 6, 7].Value = diary.is_done;

            //转交人
            _workSheet.Cells[RowToInsert + 6, 8].Value = "转交人";
            _workSheet.Cells[RowToInsert + 6, 9].Value = diary.handle_to;

            _workSheet.Cells[RowToInsert, 1].Value = "维护";
            setRange(RowToInsert, 1, RowToInsert + 6, 1);
        }


        public void InsertItem(job_item diary)
        {
            ExcelRange range;

            //项目
            _workSheet.InsertRow(RowToInsert, 4);
            _workSheet.Cells[RowToInsert, 2].Value = diary.title;
            range = _workSheet.Cells[RowToInsert, 2, RowToInsert + 4 - 1, 2];
            range.Merge = true;
            range.Style.WrapText = true;
            range.Style.ShrinkToFit = true;
            range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            var border = range.Style.Border;
            border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

            //项目内容
            _workSheet.Cells[RowToInsert, 3].Value = diary.description;
            range = _workSheet.Cells[RowToInsert, 3, RowToInsert + 4 - 1, 5];
            range.Style.WrapText = true;
            range.Merge = true;
            range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            border = range.Style.Border;
            border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;


            //完成状态
            _workSheet.Cells[RowToInsert, 6].Value = diary.done_status;
            range = _workSheet.Cells[RowToInsert, 6, RowToInsert + 4 - 1, 8];
            range.Merge = true;
            range.Style.WrapText = true;
            range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            border = range.Style.Border;
            border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;


            //预计增时
            _workSheet.Cells[RowToInsert, 9].Value = diary.cause;
            range = _workSheet.Cells[RowToInsert, 9, RowToInsert + 4 - 1, 9];
            range.Merge = true;
            range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            border = range.Style.Border;
            border.Bottom.Style = border.Top.Style = border.Left.Style = ExcelBorderStyle.Thin;
            //border.Right.Style = ExcelBorderStyle.Thick;


            _workSheet.Cells[RowToInsert, 10].Value = diary.add_time;
            range = _workSheet.Cells[RowToInsert, 9, RowToInsert + 4 - 1, 9];
            range.Merge = true;
            range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            border = range.Style.Border;
            border.Bottom.Style = border.Top.Style = border.Left.Style = ExcelBorderStyle.Thin;
            border.Right.Style = ExcelBorderStyle.Thick;


            //左边线
            range = _workSheet.Cells[RowToInsert, 1, RowToInsert + 4 - 1, 1];
            border = range.Style.Border;
            border.Left.Style = ExcelBorderStyle.Thick;




        }

        public void setTitle(string depart, string date, string user_id, string user_name)
        {
            SetDate(date);
            SetDepart(depart);
            SetNumber(user_id);
            SetStaffName(user_name);
        }


        public void Merge(int fromRow, int fromCol, int toRow, int toCol)
        {
            _workSheet.Cells[fromRow, fromCol, toRow, toCol].Merge = true;
        }

        public void Save()
        {
            _excelPackage.Save();
        }

        public void SaveAs(string filename)
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(filename);
            _excelPackage.SaveAs(fi);
        }

        public void Dispose()
        {
            _excelPackage.Dispose();
        }
    }
}
