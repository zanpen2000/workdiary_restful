﻿using MongoDB;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace excel_helper
{
    public class depart
    {
        public string id { get; set; }

        public string name { get; set; }
        public string flag { get; set; }

        public Oid Id { get; set; }

        public string manager_mail { get; set; }
    }
}
