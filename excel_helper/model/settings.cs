﻿using MongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace excel_helper
{
    public class settings
    {
        public Oid Id { get; set; }
        public string name { get; set; }
        public string value { get; set; }
        public string description { get; set; }
    }
}
