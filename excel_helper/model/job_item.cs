﻿using MongoDB;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace excel_helper
{
    public class job_item
    {
        public Oid Id { get; set; }
        public string title { get; set; }
        public string  description { get; set; }
        public string done_status { get; set; }
        public string user_id { get; set; }
        public string build { get; set; }
        public string date { get; set; }

        public string add_time { get; set; }
        public string cause { get; set; }

    }

    public class m_job_item
    {
        public Oid Id { get; set; }
        public string user_id { get; set; }
        public string build { get; set; }
        public string date { get; set; }

        public string number { get; set; }
        public string m_type { get; set; }
        public string client { get; set; }
        public string project_name { get; set; }
        public string client_time { get; set; }
        public string handle_content { get; set; }
        public string handle_method { get; set; }
        public string time_use { get; set; }
        public string is_done { get; set; }
        public string handle_to { get; set; }
        public string client_question { get; set; }
        public string contract_man { get; set; }
        public string contract_way { get; set; }

    }
}
