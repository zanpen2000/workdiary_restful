﻿using MongoDB;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace excel_helper
{
    public class users
    {
        public Oid Id { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string mail { get; set; }
        public string mailpwd { get; set; }
        public string mail2 { get; set; }
        public string depart { get; set; }
        public string prefix_depart { get; set; }
        public DateTime date { get; set; }
    }
}
