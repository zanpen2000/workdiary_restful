﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Linq;
using MongoDB;
using AppLayer;

namespace excel_helper
{
    class Program
    {
        static void Main(string[] args)
        {

            if (args.Length != 2)
            {
                Console.WriteLine("输入工号和日期(xxxx-xx-xx)作为程序的参数");
            }

            try
            {
                string number = args[0];
                string datestr = args[1];

                Console.WriteLine(datestr);

                DateTime date = Convert.ToDateTime(args[1]);
                DateTime date2 = date.AddDays(1);

                //string number = "042";
                //string datestr = "2016-6-24";
                //DateTime date2 = date.AddDays(1);

                var client = new Mongo(AppSettings.Get("connectionString"));
                client.Connect();
                var db = client.GetDatabase(AppSettings.Get("database"));

                var jobs = db.GetCollection<job_item>("jobs");
                var m_jobs = db.GetCollection<m_job_item>("m_jobs");
                var depart = db.GetCollection<depart>("depart");
                var users = db.GetCollection<users>("users");
                var settings = db.GetCollection<settings>("settings");
                var excels = db.GetCollection<excel>("excels");
               
                var job_items = jobs.Find(j => j.user_id == number && j.date == datestr);
                var m_job_items = m_jobs.Find(j => j.user_id == number && j.date == datestr);

                if (job_items.Documents.Count() < 1 && m_job_items.Documents.Count() < 1) {

                    Console.WriteLine(string.Format("没有找到日期为 {0} 的日志记录", datestr));
                    Environment.Exit(1);
                
                }

                var user = users.FindOne(u => u.id == number);
                //var depart_flag = depart.FindOne(d => d.id == user.depart).flag;
                var depart_flag = users.FindOne(d => d.id == number).prefix_depart;
                var depart_name = users.FindOne(d => d.id == number).depart;

                var diary_path = settings.FindOne(s => s.name == "diaryPath").value;
                string filename = depart_flag + number + "_" + Convert.ToDateTime(datestr).ToString("yyyyMMdd") + ".xlsx";
                string path = string.Format(diary_path, number);
                string filepath = System.IO.Path.Combine(path, filename);

                if (!System.IO.Directory.Exists(filepath))
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(filepath));

                if (System.IO.File.Exists(filepath)) System.IO.File.Delete(filepath);

                string template = settings.FindOne(s => s.name == "templatefile").value;

                template = System.IO.Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, template);

                if (!System.IO.File.Exists(template))
                {
                    Console.WriteLine("模板文件不存在: " + template );
                    Environment.Exit(1);
                }

                using (DiaryExcelHelper dh = new DiaryExcelHelper(template))
                {
                    dh.RowToInsert = int.Parse(settings.FindOne(s => s.name == "rowToInsert").value);
                    dh.RowsOfEachItem = int.Parse(settings.FindOne(s => s.name == "rowsOfEachItem").value);
                    dh.NumberPosition = settings.FindOne(s => s.name == "numPos").value;
                    dh.NamePosition = settings.FindOne(s => s.name == "namePos").value;
                    dh.DepartPosition = settings.FindOne(s => s.name == "departPos").value;
                    dh.DatePosition = settings.FindOne(s => s.name == "datePos").value;

                    int j = dh.RowToInsert;
                    foreach (var item in job_items.Documents)
                    {
                        j += dh.RowsOfEachItem;
                        dh.InsertItem(item);

                        item.build = "1";
                        jobs.Save(item);
                    }

                    dh.Merge(dh.RowToInsert - 1, 1, j - 1, 1);

                    int each = int.Parse(settings.FindOne(s => s.name == "rowsOfEachItem").value);
                    dh.RowToInsert = job_items.Documents.Count() * each + int.Parse(settings.FindOne(s => s.name == "rowToInsert").value);
                    dh.RowsOfEachItem = 1;

                    j = dh.RowToInsert;
                    foreach (var item in m_job_items.Documents.OrderByDescending(x=>x.number))
                    {
                        j += dh.RowsOfEachItem;
                        dh.Insert_m_item(item);

                        var current_row = dh.RowToInsert;

                        dh.setBorder(current_row, 1, current_row + 6, 1, "left", OfficeOpenXml.Style.ExcelBorderStyle.Thick);
                        dh.setBorder(current_row, 9, current_row + 6, 9, "right", OfficeOpenXml.Style.ExcelBorderStyle.Thick);
                        dh.setBorder(current_row, 1, current_row, 9, "top", OfficeOpenXml.Style.ExcelBorderStyle.Double);

                        item.build = "1";
                        m_jobs.Save(item);

                        current_row += 6;
                    }



                    dh.setTitle(depart_name, datestr, "N" + number, user.name);
                    
                    dh.SaveAs(filepath);

                    if (System.IO.File.Exists(filepath))
                        System.Environment.ExitCode = 0;
                    else
                        System.Environment.ExitCode = -1;
                }

                    excel xl = new excel()
                    {
                        user_id = number,
                        filepath = filepath,
                        sent = "0",
                        date = datestr
                    };

                    excels.Save(xl);
            }
            catch (Exception err)
            {
                Console.WriteLine(err.StackTrace);
                Console.WriteLine(err.Message);
                System.Environment.ExitCode = 1;

            }


        }
    }
}
