﻿
var restify = require('restify');
var router = require('./routers/index.js');

var server = restify.createServer({
    name: 'work_diary_rest_server',
    versions: ['1.0.0']
});

restify.defaultResponseHeaders = function (data) {
    this.header('Access-Control-Allow-Origin', '*');
}
//restify.defaultResponseHeaders = false;

server.use(restify.queryParser());
server.use(restify.bodyParser({ uploadDir: './app_data' }));
server.use(restify.CORS());

//depart crud api
var DEPART = '/departs';
server.get({ path: DEPART, version: router.version }, router.findAllDeparts);
server.get({ path: DEPART + '/:id', version: router.version }, router.findDepart);
server.post({ path: DEPART , version: router.version }, router.postNewDepart);
server.del({ path: DEPART + '/:id', version: router.version }, router.deleteDepart);
server.put({ path: DEPART + '/:id', version: router.version }, router.updateDepart);


//users crud api
var USERS = '/users';
server.get({ path: USERS, version: router.version }, router.findAllUsers);
server.get({ path: USERS + '/:userId', version: router.version }, router.findUser);
server.get({ path: USERS + '/userid' + '/:userId', version: router.version }, router.findUserByUserId);
server.post({ path: USERS , version: router.version }, router.saveUser);
server.del({ path: USERS + '/:userId', version: router.version }, router.deleteUser);

//jobs crud api
var PATH = '/jobs';
server.get({ path: PATH, version: router.version }, router.findAllJobs);
server.get({ path: PATH + '/nobuild/:userid/:date', version: router.version }, router.findNoBuildJobs);
server.get({ path: PATH + '/build/:userid/:date', version: router.version }, router.findBuildJobs);
server.get({ path: PATH + '/:jobId', version: router.version }, router.findJob);
server.post({ path: PATH, version: router.version }, router.postNewJob);
server.del({ path: PATH + '/:jobId', version: router.version }, router.deleteJob);
server.put({ path: PATH + '/:jobId', version: router.version }, router.updateJob);

//settings crud api
var SETTINGS = '/settings';
server.get({ path: SETTINGS, version: router.version }, router.findAllSettings);
server.get({ path: SETTINGS + '/:name', version: router.version }, router.findSettingByName);

//excel file
var EXCEL = '/excel';
server.post({ path: EXCEL , version: router.version }, router.do_excel);

//excel table
var ex = '/excelfile';
server.get({ path: ex, version: router.version }, router.getAllExcels);
server.get({ path: ex + '/nosent', version: router.version }, router.getNoSentExcels);
server.get({ path: ex + '/sent', version: router.version }, router.getSentExcels);
server.get({ path: ex + '/:user_id' + '/nosent', version: router.version }, router.getUserNoSentExcels);
server.get({ path: ex + '/:user_id' + '/sent', version: router.version }, router.getUserSentExcels);
server.get({ path: ex + '/:user_id' + '/:date' + '/nosent', version: router.version }, router.getUserNoSentExcelsByDate);
server.get({ path: ex + '/:user_id' + '/:date' + '/sent', version: router.version }, router.getUserSentExcelsByDate);
server.get({ path: ex + '/count/sent/:user_id/:date', version : router.version }, router.count_day_mailed);
server.get({path:ex + '/download/' + ':id', version:router.version}, router.downloadExcelFile);

//test
server.get({ path: '/test', vsersion: router.version }, router.test);

//version check and update

var upt = '/update';
server.post({ path: 'up', version: router.version }, router.uploadapk);
server.post({ path: upt + '/check', version: router.version }, router.updateCheck);
server.get({ path: upt + '/download', version: router.version }, router.getApk);

var fs = require('fs');

server.get({ path: upt + '/upload', version: router.version }, function (req, res, next) {
    
    if (fs.existsSync('./up.html')) {
        
        var filestream = fs.createReadStream('./up.html');
        filestream.pipe(res);
        next();

    }
    else {
        res.statusCode = 404;
        res.write("404 sorry not found");
        res.end();
        next();
    }

});

//about
server.get({ path: '/about', version: router.version }, router.about);

server.listen(process.env.PORT || 9889, function () {
    console.log('%s listening at %s ', server.name, server.url);
});

server.get({path:'/diary', version:router.version}, router.newDiary);