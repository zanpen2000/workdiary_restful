﻿var mongojs = require('mongojs');

var connection_string = '192.168.2.123:27017/work_diary_rest_server';
var db = mongojs(connection_string, ['work_diary_rest_server']);
var jobs = db.collection('jobs');
var users = db.collection('users');
var settings = db.collection('settings');
var departs = db.collection('depart');
var excels = db.collection('excels');
var update = db.collection('update');

exports.jobs = jobs;
exports.users = users;
exports.settings = settings;
exports.departs = departs;
exports.excels = excels;
exports.update = update;