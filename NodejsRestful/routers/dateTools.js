﻿Date.prototype.pattern = function (A) {
    var D = {
        "M+" : this.getMonth() + 1,
        "d+" : this.getDate(),
        "h+" : this.getHours() % 12 == 0 ? 12 : this.getHours() % 12,
        "H+" : this.getHours(),
        "m+" : this.getMinutes(),
        "s+" : this.getSeconds(),
        "q+" : Math.floor((this.getMonth() + 3) / 3),
        "S" : this.getMilliseconds()
    };
    var B = {
        "0" : "/u65e5",
        "1" : "/u4e00",
        "2" : "/u4e8c",
        "3" : "/u4e09",
        "4" : "/u56db",
        "5" : "/u4e94",
        "6" : "/u516d"
    };
    if (/(y+)/.test(A)) {
        A = A.replace(RegExp.$1, (this.getFullYear() + "")
.substr(4 - RegExp.$1.length))
    }
    if (/(E+)/.test(A)) {
        A = A
.replace(
            RegExp.$1,
((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "/u661f/u671f"
: "/u5468")
: "") 
+ B[this.getDay() + ""])
    }
    for (var C in D) {
        if (new RegExp("(" + C + ")").test(A)) {
            A = A.replace(RegExp.$1, (RegExp.$1.length == 1) ? (D[C])
: (("00" + D[C]).substr(("" + D[C]).length)))
        }
    }
    return A
};
Date.prototype.isLeapYear = function () {
    return (0 == this.getYear() % 4 && ((this.getYear() % 100 != 0) || (this
.getYear() % 400 == 0)))
};
Date.prototype.Format = function (C) {
    var B = C;
    var A = ['日', '一', '二', '三', '四', '五', '六'];
    B = B.replace(/yyyy|YYYY/, this.getFullYear());
    B = B.replace(/yy|YY/, (this.getYear() % 100) > 9 ? (this.getYear() % 100)
.toString() : "0" + (this.getYear() % 100));
    B = B.replace(/MM/, this.getMonth() > 9 ? this.getMonth().toString() : "0" 
+ this.getMonth());
    B = B.replace(/M/g, this.getMonth());
    B = B.replace(/w|W/g, A[this.getDay()]);
    B = B.replace(/dd|DD/, this.getDate() > 9 ? this.getDate().toString() : "0" 
+ this.getDate());
    B = B.replace(/d|D/g, this.getDate());
    B = B.replace(/hh|HH/, this.getHours() > 9 ? this.getHours().toString()
: "0" + this.getHours());
    B = B.replace(/h|H/g, this.getHours());
    B = B.replace(/mm/, this.getMinutes() > 9 ? this.getMinutes().toString()
: "0" + this.getMinutes());
    B = B.replace(/m/g, this.getMinutes());
    B = B.replace(/ss|SS/, this.getSeconds() > 9 ? this.getSeconds().toString()
: "0" + this.getSeconds());
    B = B.replace(/s|S/g, this.getSeconds());
    return B
};
function daysBetween(A, I) {
    var B = A.substring(5, A.lastIndexOf("-"));
    var F = A.substring(A.length, A.lastIndexOf("-") + 1);
    var C = A.substring(0, A.indexOf("-"));
    var D = I.substring(5, I.lastIndexOf("-"));
    var G = I.substring(I.length, I.lastIndexOf("-") + 1);
    var E = I.substring(0, I.indexOf("-"));
    var H = ((Date.parse(B + "/" + F + "/" + C) - Date.parse(D + "/" + G + "/" 
+ E)) / 86400000);
    return Math.abs(H)
}
Date.prototype.DateAdd = function (A, B) {
    var C = this;
    switch (A) {
        case "s":
            return new Date(C.getTime() + (1000 * B));
        case "n":
            return new Date(C.getTime() + (60000 * B));
        case "h":
            return new Date(C.getTime() + (3600000 * B));
        case "d":
            return new Date(C.getTime() + (86400000 * B));
        case "w":
            return new Date(C.getTime() + ((86400000 * 7) * B));
        case "q":
            return new Date(C.getFullYear(), (C.getMonth()) + B * 3, C.getDate(), C
.getHours(), C.getMinutes(), C.getSeconds());
        case "m":
            return new Date(C.getFullYear(), (C.getMonth()) + B, C.getDate(), C
.getHours(), C.getMinutes(), C.getSeconds());
        case "y":
            return new Date((C.getFullYear() + B), C.getMonth(), C.getDate(), C
.getHours(), C.getMinutes(), C.getSeconds())
    }
};
Date.prototype.DateDiff = function (A, B) {
    var C = this;
    if (typeof B == "string") {
        B = StringToDate(B)
    }
    switch (A) {
        case "s":
            return parseInt((B - C) / 1000);
        case "n":
            return parseInt((B - C) / 60000);
        case "h":
            return parseInt((B - C) / 3600000);
        case "d":
            return parseInt((B - C) / 86400000);
        case "w":
            return parseInt((B - C) / (86400000 * 7));
        case "m":
            return (B.getMonth() + 1) + ((B.getFullYear() - C.getFullYear()) * 12) 
- (C.getMonth() + 1);
        case "y":
            return B.getFullYear() - C.getFullYear()
    }
};
function StringToDate(C) {
    var A = Date.parse(C);
    var B = new Date(A);
    if (isNaN(B)) {
        var D = C.split("-");
        B = new Date(D[0], --D[1], D[2])
    }
    return B
}
function DateAdd(C, A, B) {
    switch (A) {
        case "s":
            return new Date(C.getTime() + (1000 * B));
        case "n":
            return new Date(C.getTime() + (60000 * B));
        case "h":
            return new Date(C.getTime() + (3600000 * B));
        case "d":
            return new Date(C.getTime() + (86400000 * B));
        case "w":
            return new Date(C.getTime() + ((86400000 * 7) * B));
        case "q":
            return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + B * 3, dtTmp
.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp
.getSeconds());
        case "m":
            return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + B, dtTmp
.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp
.getSeconds());
        case "y":
            return new Date((dtTmp.getFullYear() + B), dtTmp.getMonth(), dtTmp
.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp
.getSeconds())
    }
}
function DateDiff(A, C, B) {
    if (typeof B == "string") {
        B = StringToDate(B)
    }
    switch (A) {
        case "s":
            return parseInt((B - C) / 1000);
        case "n":
            return parseInt((B - C) / 60000);
        case "h":
            return parseInt((B - C) / 3600000);
        case "d":
            return parseInt((B - C) / 86400000);
        case "w":
            return parseInt((B - C) / (86400000 * 7));
        case "m":
            return (B.getMonth() + 1) + ((B.getFullYear() - C.getFullYear()) * 12) 
- (C.getMonth() + 1);
        case "y":
            return B.getFullYear() - C.getFullYear()
    }
}
Date.prototype.toString = function (B) {
    var A = this;
    var D = A.toLocaleDateString();
    if (B) {
        var C = ['日', '一', '二', '三', '四', '五', '六'];
        D += " 星期" + C[A.getDay()]
    }
    return D
};
function CheckDateTime(C) {
    var B = /^(\d+)-(\d{ 1,2 })-(\d{ 1,2 }) (\d{ 1,2 }):(\d{ 1,2 }):(\d{ 1,2 })$/;
    var A = C.match(B);
    if (A == null) {
        return false
    }
    A[2] = A[2] - 1;
    var D = new Date(A[1], A[2], A[3], A[4], A[5], A[6]);
    if (D.getFullYear() != A[1]) {
        return false
    }
    if (D.getMonth() != A[2]) {
        return false
    }
    if (D.getDate() != A[3]) {
        return false
    }
    if (D.getHours() != A[4]) {
        return false
    }
    if (D.getMinutes() != A[5]) {
        return false
    }
    if (D.getSeconds() != A[6]) {
        return false
    }
    return true
}
Date.prototype.toArray = function () {
    var A = this;
    var B = Array();
    B[0] = A.getFullYear();
    B[1] = A.getMonth();
    B[2] = A.getDate();
    B[3] = A.getHours();
    B[4] = A.getMinutes();
    B[5] = A.getSeconds();
    return B
};
Date.prototype.DatePart = function (C) {
    var A = this;
    var D = "";
    var B = ['日', '一', '二', '三', '四', '五', '六'];
    switch (C) {
        case "y":
            D = A.getFullYear();
            break;
        case "m":
            D = A.getMonth() + 1;
            break;
        case "d":
            D = A.getDate();
            break;
        case "w":
            D = B[A.getDay()];
            break;
        case "ww":
            D = A.WeekNumOfYear();
            break;
        case "h":
            D = A.getHours();
            break;
        case "n":
            D = A.getMinutes();
            break;
        case "s":
            D = A.getSeconds();
            break
    }
    return D
};
Date.prototype.MaxDayOfDate = function () {
    var C = this;
    var B = C.toArray();
    var E = (new Date(B[0], B[1] + 1, 1));
    var D = E.DateAdd(1, "m", 1);
    var A = DateDiff(E.Format("yyyy-MM-dd"), D.Format("yyyy-MM-dd"));
    return A
};
function StringToDate(C) {
    var A = Date.parse(C);
    var B = new Date(A);
    if (isNaN(B)) {
        var D = C.split("-");
        B = new Date(D[0], --D[1], D[2])
    }
    return B
}
function DateCompare(C, A) {
    var B = Date.parse(C) - Date.parse(A);
    if (B == 0) {
        return 0
    } else {
        if (B < 0) {
            return 1
        } else {
            if (B > 0) {
                return -1
            }
        }
    }
}
function isDate(A) {
    if (A.trim() == "") {
        return true
    }
    var C = A.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);
    if (C == null) {
        return false
    }
    var B = new Date(C[1], C[3] - 1, C[4]);
    var D = (B.getFullYear() == C[1] && (B.getMonth() + 1) == C[3] && B
.getDate() == C[4]);
    if (D == 0) {
        return false
    }
    return (D != 0)
};