﻿
var cry = require('crypto');

var k = 'tsdykjhz';



var encode = function (text, key) { 

    var cipher = cry.createCipher('aes-256-cbc', key);
    //var cipher = cry.createCipher('des', key);
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}

var decode = function (text, key) {
    var decipher = cry.createDecipher('aes-256-cbc', key);
    //var decipher = cry.createDecipher('des', key);
    var dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec; 
}


exports.encode = encode;
exports.decode = decode;

//console.log(encode('123456',k));