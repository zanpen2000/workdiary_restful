#coding=utf-8
from dbobject import *
import bson

settings.remove()

s = [
  {
    "name":"companyName",
    "value":"唐山市达意科技有限公司",
    "description":"公司名称"
  },
  {
    "name":"rowToInsert",
    "value":"9",
    "description":"NULL"
  },
  {
    "name":"datePos",
    "value":"G3",
    "description":"NULL"
  },
  {
    "name":"namePos",
    "value":"B3",
    "description":"NULL"
  },
  {
    "name":"departPos",
    "value":"C5",
    "description":"NULL"
  },
  {
    "name":"numPos",
    "value":"E3",
    "description":"NULL"
  },
  {
    "name":"templatefile",
    "value":"App_Data\\Template.xlsx",
    "description":"NULL"
  },
  {
    "name":"diaryPath",
    "value":"App_Data\\{0}",
    "description":"NULL"
  },

  {
    "name":"rowsOfEachItem",
    "value":"4",
    "description":"NULL"
  }
  ,

  {
    "name":"excel_generater_path",
    "value":"D:\\sources\\nodejs\\NodejsRestful\\excel_helper\\bin\\Debug\\excel_helper.exe",
    "description":"Excel文件生成工具路径"
  }
]

s.append({"name":"author","value":"zp","description":"null"})

settings.insert(s)
