#coding=utf-8
from pymongo import MongoClient

host = '127.0.0.1';
port = 27017

client = MongoClient(host, port)

db = client['work_diary_rest_server']

settings = db['settings']
users = db['users']
jobs = db['jobs']
excels = db['excels'];
update = db['update'];
