#coding:utf-8


class job_bean(object):

    def __init__(self, id, title, desc, status, date, uid):

        self.id = id
        self.user_id = uid
        self.title = title
        self.description = desc
        self.done_status = status
        self.date = date

    def __repr__(self):
        return '%s,%s,%s,%s,%s,%s' % (self.id, self.title, self.description, self.done_status, self.date, self.user_id)
