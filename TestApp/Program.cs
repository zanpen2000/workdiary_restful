﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using _3rd;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var s1 = Security.Encode("123456");

            Console.WriteLine(s1);

            var s2 = Security.Decode(s1);

            Console.WriteLine(s2);

            Console.ReadKey();

        }
    }
}
